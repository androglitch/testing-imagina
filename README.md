# Testing imagina formación
Red social imitando a Twitter para la formación de testing.

## Contexto

Actualmente en el proyecto se encuentra realizado el registro de usuarios, el login, el listado de tweets propios y la publicación de un nuevo tweet

## A realizar


- Buscar usuarios.
- Modificar datos del perfil.
- Obtener el listado de a quién sigo.
- Obtener el listado de quién me sigue.
- Crear la opción de seguir.


- Crear un tweet
- Buscar tweets.
- Crear la opción de retweet.
- Crear la opción de comentarios.


## Por probar

Actualizar Perfil
Actualizar Perfil fallido
	- Datos inválidos
	- Sin datos


Buscar usuarios devuelve usuarios

Comprobar que al crear un tweet se crea con el usuario correcto.
Comprobar que no se puede crear un tweet por otro usuario.
Comprobar el borrado correcto de un tweet.
Comprobar el borrado incorrecto de un tweet
	-- que no exista
	-- que no sea tuyo


Comprobar que al seguir a alguien se crea con tu identificador.
Comprobar que el retweet aparece en tu perfil.
Comprobar que se pueden realizar comentarios.
Comprobar que no se puede comentar en un tweet eliminado
Comprobar el funcionamiento de deshacer retweet
	-- (No aparece en tu perfil)


Borrar Perfil
Borrar Perfil fallido

Logout



## Probado

Login funciona
Fallo de login funciona

Registro funciona
Fallo de registro funciona

Publicación de un nuevo tweet

Dar like funciona
	-- ¿Qué ocurre si le doy dos veces? -> Quitar el like
	-- ¿Si lo quito desaparece de mi perfil?
