{{--
	
@servers([
	'deploy_server' => 'user@192.167.46.15',
	'second' => 'seconduser@194....'
])

--}}

@servers(['localhost' => '127.0.0.1', 'localhost2' => '192.168.0.14'])

@task('example')
	echo {{ $variable }}
@endtask

@setup
	$today = date('YmdHis');
	$repository = "https://gitlab.com/androglitch/testing-imagina.git"
@endsetup

@story('deploy', ['on' => ['localhost']])
	clone_repository
	run_composer
	run_npm
@endstory

@task('clone_repository')
	mkdir clone{{ $today }}
	cd clone{{ $today }}
	git clone {{ $repository }}
@endtask

@task('run_composer')
	composer install --prefer-dist --no-scripts -q -o
@endtask

@task('run_npm')
	npm install
	npm run prod
@endtask

@task('generate_dotenv')
	
@endtask