<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [App\Http\Controllers\API\AuthController::class, 'register']);
Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login'])->name('api.login');

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);
    
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', [App\Http\Controllers\API\ProfileController::class, 'get']);
        Route::get('/likes', [App\Http\Controllers\API\ProfileController::class, 'likes']);
        Route::post('/', [App\Http\Controllers\API\ProfileController::class, 'store']);
        Route::post('/update-avatar', [App\Http\Controllers\API\ProfileController::class, 'avatar']);
        Route::delete('/', [App\Http\Controllers\API\ProfileController::class, 'remove']);
    });

    Route::group(['prefix' => 'tweets'], function () {
        Route::get('/', [App\Http\Controllers\API\TweetController::class, 'get']);
        Route::get('/search', [App\Http\Controllers\API\TweetController::class, 'get']);
        Route::post('/{identifier?}', [App\Http\Controllers\API\TweetController::class, 'store'])->whereUuid('identifier');
        Route::post('/retweet/{identifier}', [App\Http\Controllers\API\TweetController::class, 'retweet'])->whereUuid('identifier');
        Route::post('/comment/{identifier}', [App\Http\Controllers\API\TweetController::class, 'comment']);
        Route::delete('/{identifier}', [App\Http\Controllers\API\TweetController::class, 'delete'])->whereUuid('identifier');
    });

    Route::group(['prefix' => 'likes'], function () {
        Route::post('/toggle/{identifier}', [App\Http\Controllers\API\LikesController::class, 'toggle']);
    });

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [App\Http\Controllers\API\UsersController::class, 'search']);
        Route::get('/following/{user?}', [App\Http\Controllers\API\FollowingController::class, 'following']);
        Route::get('/follower/{user?}', [App\Http\Controllers\API\FollowingController::class, 'follower']);
        Route::post('/follow/{user}', [App\Http\Controllers\API\FollowingController::class, 'follow']);
        Route::delete('/follow/{user}', [App\Http\Controllers\API\FollowingController::class, 'unfollow']);
    });

});