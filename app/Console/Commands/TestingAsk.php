<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TestingAsk extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testing:ask';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $age = $this->ask('¿Cuántos años tienes?');
        $name = $this->ask('¿Cuál es tu nombre?');
        $language = $this->choice('¿Con qué lenguaje programas?',[
            'PHP',
            'C',
            'Python'
        ]);
        $this->line("Tu nombre es $name te gusta $language y tienes $age años");
        $this->confirm('¿Es correcto?');
        
        $this->line("Este es mi segundo line");
        return 0;
    }
}
