<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Str;

class CreateNewTweetCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:tweet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->line("Welcome to tweet creation command");
        $username = $this->ask("What's the username of the owner");
        $content = $this->ask("Write the content of the tweet");
        $identifier = Str::uuid();
        $user = User::where('username',$username)->first();
        $user->tweets()->create([
            'identifier' => $identifier,
            'content' => $content
        ]);

        $this->line("New tweet created properly");
        return 0;
    }
}
