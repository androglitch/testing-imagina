<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Validator;

class CreateNewUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->line("Welcome to user creation command");
        $description = null;
        do{
            $email = $this->ask("What's the email of the user");
            $username = $this->ask("What's the username of the user");
            $alreadyExists = User::where('email',$email)->orWhere('username',$username)->exists();
            if($alreadyExists)
                $this->line("User with that email or username already exists");
            
        }while($alreadyExists);

        $password = $this->ask("What's the password of the user");

        $name = $this->ask("What's the name of the user");
        $surname = $this->ask("What's the surname of the user");
        if($this->confirm("Do you want to add any description?")){
            $description = $this->ask("What's the description of the user");
        }

        $userDetails = [
            'description' => $description,
            'email' => $email,
            'username' => $username,
            'password' => \Hash::make($password),
            'name' => $name,
            'surname' => $surname
        ];

        $validator = Validator::make(
            $userDetails,
            [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:8'
            ]
        );
        if($validator->fails())
            return 1;


        $newUser = User::create($userDetails);
        $this->line("New user created properly");
        return 0;
    }
}
