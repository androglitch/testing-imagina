<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tweet;
use App\Models\Comment;
use App\Classes\TestClass;
use Auth;
use Str;
use Validator;
use Mail;
use App\Mail\OrderMail;
use App\Http\Resources\TweetResource;

class TweetController extends Controller
{

    public function get(Request $request){
        $perPage = $request->get('perPage')??8;
        $content = $request->get('search')??null;
        $tweets = Tweet::whereIn('user_id', Auth::user()->following->pluck('id'))
                    ->orWhere('user_id', Auth::id())
                    ->with(['user'])
                    ->latest()
                    ->paginate($perPage);



        return response()->json($tweets);
    }

    public function store(Request $request, $identifier = null){

        $statusCode = $identifier==null?201:200;
        $input = $request->input();
        $input['identifier'] = $identifier??(string)Str::uuid();
        $validation = Validator::make($input,
            [
                'content' => 'required|string|min:1',
                'identifier' => 'required|string|max:255' 
            ],[
                'content.required' => 'No se puede crear un tweet vacío',
                'content.string' => 'El contenido debe contener texto',
                'content.min' => 'No se puede crear un tweet vacío',
                'identifier.required' => 'El identificador es obligatorio',
                'identifier.string' => 'El identificador es debe ser un texto',
                'identifier.max' => 'Se ha excedido el tamaño del identificador',
            ]
        );

        if($validation->fails()){
            return response(json_encode($validation->errors()), 401);
        }

        if($identifier != null && !Tweet::where('identifier', $identifier)->exists()){
            return response(null, 404);
        }

        $newOne = Auth::user()->tweets()->updateOrCreate([
            'identifier' => $input['identifier']
        ], $input);
        $response = new TweetResource($newOne);
        return response(json_encode($response), $statusCode);
    }

    public function retweet($identifier){
        $tweet = Tweet::where('identifier',$identifier)->first();
        if(!$tweet)
            return response(null, 404);

        $cloned = $tweet->replicate();
        $cloned->identifier = Str::uuid();
        $cloned->user_id = Auth::id();
        $cloned->save();
        return response(json_encode($cloned), 201);
    }

    public function comment(Request $request, $identifier){
        $input = $request->input();
        $validation = Validator::make($input, [
            'content' => 'required',
        ],[
            'content.required' => 'Debes especificar un contenido'
        ]);
        if($validation->fails())
            return response(json_encode($validation->errors()), 401);
        
        $tweet = Tweet::where('identifier',$identifier)->first();
        $input['user_id'] = Auth::id();
        $comment = new Comment($input);
        $tweet->comments()->save($comment);
        return response(json_encode($comment), 201);
    }

    public function delete(Request $request, $identifier){
        $tweet = Tweet::where('identifier', $identifier)->where('user_id', Auth::id())->first();
        if(!$tweet)
            return response(null, 404);

        $tweet->delete();
        return response(null, 204);
    }
}