<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tweet;
use Validator;
use Hash;
use Storage;
use Auth;

class ProfileController extends Controller
{
    public function get(Request $request){
        return auth()->user();
    }

    public function store(Request $request){
        $input = $request->input();
        $validation = Validator::make($input,
            [
                'name' => 'required|string|min:1',
                'description' => 'string|max:255',
                'password' => 'min:8'
            ],[
                'name.required' => 'Debes especificar tu nombre',
                'description.string' => 'La descripción debe contener texto',
                'description.max' => 'Se ha excedido el tamaño de la descripción',
                'password.min' => 'La contraseña debe tener al menos 8 caracteres',
            ]
        );
        if($validation->fails())
            return response(json_encode($validation->errors()), 401);

        if(isset($input['password']))
            $input['password'] = Hash::make($input['password']);

        Auth::user()->update($input);
    }

    public function likes(){
        $likes = auth()->user()->likes->pluck('tweet_id')->toArray();
        $tweets =Tweet::whereIn('id', $likes)->get();
        return response(json_encode($tweets));
    }

    public function avatar(Request $request){
        $avatar = $request->file('avatar');
        $path = Storage::putFile('avatars', $avatar);
        Auth::user()->update(['avatar' => $path]);
        return auth()->user();
    }

    public function remove(){
        $user = Auth::user();
        Auth::logout();
        //$user->tweets->delete();
        $user->delete();
    }
}
