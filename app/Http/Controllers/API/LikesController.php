<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Like;
use App\Models\Tweet;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;

class LikesController extends Controller
{
    public function toggle(Request $request, $identifier){
        $tweet = Tweet::where('identifier', $identifier)->first();
        if(!$tweet)
            return response(json_encode(['message' => 'El tweet no existe']), 401);

        $idUser = Auth::id();
        $like = Like::firstOrNew([
            'user_id' => $idUser,
            'tweet_id' => $tweet->id
        ]);
        if($like->id){
            $like->delete();
            return response(null, 204);
        }
        $like->save();
        return response(json_encode($like), 201);
            
        
    }

}
