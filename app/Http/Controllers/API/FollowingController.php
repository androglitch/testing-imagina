<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class FollowingController extends Controller
{
    public function search(){

    }

    public function following(User $user)
    {
        return response()->json($user->following);
    }

    public function follower()
    {
        return response()->json($user->followers);
    }


    public function follow(User $user){
        Auth::user()->follow($user);
        $follow = Auth::user()->where('user_id', $user->id)->first();
        return response(json_encode($follow), 201);
    }
    
    public function unfollow(User $user){
        Auth::user()->follow($unfollow);
        return response(null, 204);
    }
}
