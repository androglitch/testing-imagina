<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\User;

class UsersController extends Controller
{
    public function search(Request $request){
        $username = $request->input(['username']);
        $users = User::where('username', 'like', '%' . $username . '%')->get();
        return response($users->toJson(), 200);
    }
}
