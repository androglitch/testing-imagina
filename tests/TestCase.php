<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

/* Dummy comment */

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
}
