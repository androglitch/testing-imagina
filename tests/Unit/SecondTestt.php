<?php

use App\Classes\TestClass;

beforeAll(function(){
    
});

afterAll(function(){

});

beforeEach(function(){
    $this->_class = new TestClass();
});

afterEach(function(){
    
});


test('this is my first test', function () {
    $this->_class->getId();
    
})->only();


it('is my first test', function ($email) {
    expect(true)->toBeNumeric();
    expect(true)->toBeArray();
    expect(true)->toContain();
    expect(true)->toHaveCount();
    expect(true)->toCount();
})->group('database')->with([
    'pepe@example.net',
    'john@example.net'
]);