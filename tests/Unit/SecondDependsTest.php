<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

/**
 * 
 * @group repositories
 * 
 * */

class SecondDependsTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * 
     * @depends Tests\Unit\FirstDependsTest::testExample
     * @group dummy
     * @group silly
     * 
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
}
