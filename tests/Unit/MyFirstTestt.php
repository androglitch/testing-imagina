<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Classes\TestClass;
use Exception;

/**
 * 
 * @coversDefaultClass
 * */

class MyFirstTest extends TestCase
{

    private $_class;

    public function setUp(): void{
        parent::setUp();
        $this->_class = new TestClass();
    }


    public function tearDown(): void{
        unset($this->_class);
    }
 

    /**
     * 
     * @group testfalse
     * 
     * */

    public function testConstruct(){
        $this->assertTrue(true);
    }

    /**
     * 
     * @group testtrue
     * @small
     * @depends testConstruct
     * 
     * XDebug
     * */
    public function testTrue()
    {
        $this->assertTrue(true);
    }
    /**
     * 
     * @depends testConstruct
     * 
     * */


    public function testEquals(){
        $array = [
            'id' => 1,
            'name' => "Jose"
        ];
        $this->assertEquals($array, [
            'name' => "Jose",
            'id' => 1
        ]);
    }
    public function _testSame(){
        $array = [
            'id' => 1,
            'name' => "Jose"
        ];
        $this->assertSame($array, [
            'id' => 1,
            'name' => "Jose",
            'direccion' => [
                'numero' => 23,
                'calle' => 'San Fermin'
            ]
        ]);
    }

    public function testContains(){
        $array = [
            'id' => 1,
            'name' => "Jose"
        ];
        $this->assertContains("Jose", $array);
    }

    /* Revisar si hay alguna forma de evaluar estructuras complejas */

    public function _testAssetContainsKey(){
        $array = [
            'id' => 1,
            'name' => "Jose"
        ];
        $this->assertArrayHasKey(['surname'], $array);
    }

    public function testInstanceOf(){
        $this->assertInstanceOf(TestClass::class, $this->_class);
    }

    /* Comprobar con herencia */

    public function testException(){
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Exception");
        $this->_class->throwException();
    }


    /* Permitir la configuración a nivel de composer.json */
}
