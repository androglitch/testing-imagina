<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Http\Middleware\CheckThatNumberIsOneMiddleware;
use App\Http\Middleware\SetLocaleMiddleware;
use Illuminate\Http\Request;
use Exception;
use App;
/**
 * 
 * @group middleware
 * 
 * */

class MiddlewareTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $request = new Request();
        $request->merge([
            'number' => 1
        ]);
        $middleware = new CheckThatNumberIsOneMiddleware();
        $result = $middleware->handle($request, function(){
            return 4;
        });
        $this->assertSame(4, $result);
    }

    public function test_example_exception()
    {
        $this->expectException(Exception::class);
        $request = new Request();
        $request->merge([
            'number' => 2
        ]);
        $middleware = new CheckThatNumberIsOneMiddleware();
        $result = $middleware->handle($request, function(){
            return 4;
        });
    }

    public function _testChangeLocale(){
        $request = new Request();
        $request->merge([
            'locale' => 'es'
        ]);

        $middleware = new SetLocaleMiddleware();
        $result = $middleware->handle($request, function(){
            return 4;
        });

        $newLocale = App::getLocale();


        $this->assertSame('en', $newLocale);
    }
}



















