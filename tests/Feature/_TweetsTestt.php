<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Tweet;
use Str;
use Exception;


/**
 * 
 * @group tweets
 * @group http
 * 
 * */

class TweetsTestt extends TestCase
{

    /**
     * A basic feature test example.
     *
     * @return voids
     */
    private $_user;


    public function setUp(): void{
        parent::setUp();
        if(!$this->_user)
            $this->_user = User::factory()->create();
        
    }

    public function testStoreIdentifier(){
        $data = [
            'content' => Str::random(50)
        ];
        $response = $this->actingAs($this->_user)->postJson('/api/tweets', $data);        
        $content = $response->decodeResponseJson();



        $this->assertIsString($content['identifier']);
    }

    /**
     * 
     * @depends testStoreIdentifier
     * 
     * */

    public function testStore(){
        $data = [
            'content' => Str::random(50)
        ];
        $response = $this->actingAs($this->_user)->postJson('/api/tweets', $data);
        $content = $response->decodeResponseJson();
        $tweet = Tweet::where('identifier', $content['identifier'])->first()->toArray();
        $response->assertStatus(201);
        $response->assertExactJson($tweet);
        $this->assertSame($data['content'], $tweet['content']);
    }

    public function testStoreInDatabaseWithLongIdentifier(){
        $identifier = Str::random(300);
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/' . $identifier, [
            'content' => 'Lorem Ipsum',
            'identifier' => $identifier
        ]);
        $response->assertStatus(401);
    }


    public function testUpdate(){
        $tweet = Tweet::factory()->create([
            'user_id' => $this->_user->id
        ]);
        $data = [
            'content' => Str::random(500)
        ];
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/' . $tweet->identifier, $data);
        $content = $response->decodeResponseJson();
        $tweetAfterUpdate = Tweet::where('identifier', $content['identifier'])->first()->toArray();

        $this->assertSame($data['content'], $tweetAfterUpdate['content']);
        $response->assertStatus(200);
        $response->assertExactJson($tweetAfterUpdate);
    }

    public function testStoreInDatabaseWithEmptyContent(){
        $response = $this->actingAs($this->_user)->postJson('/api/tweets', [
            'content' => ''
        ]);
        $response->assertStatus(401);
    }

    public function testStoreInDatabaseWithoutContent()
    {
        $response = $this->actingAs($this->_user)->postJson('/api/tweets');
        $response->assertStatus(401);
    }

    public function testStoreWithoutUser(){
        $response = $this->postJson('/api/tweets', [
            'content' => 'Lorem Ipsum'
        ]);
        $response->assertStatus(401);
    }

    public function testUpdateFailsByIdentifier(){
        $identifier = 'asdfg';
        $data = [
            'content' => Str::random(500)
        ];
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/' . $identifier, $data);
        $response->assertStatus(404);
    }

    public function testUpdateFailsByNoContent(){
        $tweet = Tweet::factory()->create([
            'user_id' => $this->_user->id
        ]);
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/' . $tweet->identifier);
        $response->assertStatus(401);
    }
    public function testUpdateFailsByEmptyContent(){
        $tweet = Tweet::factory()->create([
            'user_id' => $this->_user->id
        ]);
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/' . $tweet->identifier, ['content' => '']);
        $response->assertStatus(401);
    }

    //- Buscar tweets.
    public function _testGettingTweets(){
        $comparision = Tweet::where('user_id',$this->_user->id)
                        ->orWhereIn('user_id',$this->_user->following->pluck('id'))
                        ->with(['user'])
                        ->latest()
                        ->paginate(8)
                        ->toArray();
        $response = $this->actingAs($this->_user)->get('/api/tweets');
        $response->assertExactJson($comparision);
    }

    
    public function testMakeRetweet(){
        $tweet = Tweet::factory()->create();
        $response = $this->actingAs($this->_user)->post('/api/tweets/retweet/' . $tweet->identifier);
        $content = $response->decodeResponseJson();
        $retweeted = Tweet::where('identifier', $content['identifier'])->where('user_id', $this->_user->id)->first();

        $this->assertInstanceOf(Tweet::class, $retweeted);
        $response->assertStatus(201);
    }

    public function testMakeRetweetFails(){
        $tweet = Tweet::factory()->create();
        $response = $this->actingAs($this->_user)->post('/api/tweets/retweet/asdaqwe');
        $response->assertStatus(404);
    }

    public function _testMakeRetweetFailsForNowIdentifier(){
        $tweet = Tweet::factory()->create();
        $response = $this->actingAs($this->_user)->post('/api/tweets/retweet');
        $response->assertStatus(404);
    }


    //- Crear la opción de comentarios.
    public function testMakeComment(){
        $tweet = Tweet::factory()->create();
        $response = $this->actingAs($this->_user)->post('/api/tweets/comment/' . $tweet->identifier,[
            'content' => Str::random(100)
        ]);
        $response->assertStatus(201);
    }

    public function testThatMakeEmptyCommentFails(){
        $tweet = Tweet::factory()->create();
        $response = $this->actingAs($this->_user)->post('/api/tweets/comment/' . $tweet->identifier);
        $response->assertStatus(401);
    }

    public function testThatMakeNoCommentFails(){
        $tweet = Tweet::factory()->create();
        $response = $this->actingAs($this->_user)->post('/api/tweets/comment/' . $tweet->identifier,[
            'content' => ''
        ]);
        $response->assertStatus(401);
    }

    public function testThatRemoveTweetWorks(){
        $tweet = Tweet::factory()->create([
            'user_id' => $this->_user->id
        ]);
        $response = $this->actingAs($this->_user)->delete('/api/tweets/' . $tweet->identifier);
        $response->assertStatus(204);
    }
    public function testThatRemoveTweetFails(){
        $response = $this->actingAs($this->_user)->delete('/api/tweets/asdaqwe');
        $response->assertStatus(404);
    }
    
    public function testThatNonYoursRemoveTweetFails(){
        $tweet = Tweet::factory()->create();
        $response = $this->actingAs($this->_user)->delete('/api/tweets/' . $tweet->identifier);
        $response->assertStatus(404);
    }

}
