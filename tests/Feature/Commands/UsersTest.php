<?php

namespace Tests\Feature\Commands;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Symfony\Component\Console\Exception\RuntimeException;
use Str;


/**
 * 
 * @group commands
 * @group users
 * 
 * */

class UsersTest extends TestCase
{
    //use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCountingUsersWorks(){
        $howManyUsers = User::count();
        $response = $this->artisan('users:count');
        $response->expectsOutput($howManyUsers);
    }

    public function testSearchUsersWorks(){
        $user = User::factory()->create();

        $users = User::where('username', 'like', '%' . $user->username . '%')->get();
        $response = $this->artisan('search:user ' . $user->username);
        $response->expectsOutput($users->toJson());
    }

    public function testSearchMultipleUsersWorks(){
        $users = User::where('username', 'like', '%n%')->get();
        $response = $this->artisan('search:user n');
        $response->expectsOutput($users->toJson());
    }

    public function testSearchUsersDontWorks(){
        $this->expectException(RuntimeException::class);
        $response = $this->artisan('search:user');
    }

    public function testCreateUser(){
        $user = User::factory()->make();
        $response = $this->artisan('create:user');
        $response->expectsOutput("Welcome to user creation command");
        $response->expectsQuestion("What's the email of the user", $user->email);
        $response->expectsQuestion("What's the username of the user", $user->username);
        $response->expectsQuestion("What's the password of the user", 'password');
        $response->expectsQuestion("What's the name of the user", $user->name);
        $response->expectsQuestion("What's the surname of the user", $user->surname);
        $response->expectsQuestion("Do you want to add any description?", false);
        $response->expectsOutput("New user created properly");
    }

    public function testCreateUserWithEmailThatExists(){
        $user = User::factory()->create();
        $correctUser = User::factory()->make();
        $response = $this->artisan('create:user');
        $response->expectsOutput("Welcome to user creation command");
        $response->expectsQuestion("What's the email of the user", $user->email);
        $response->expectsQuestion("What's the username of the user", $user->username);

        $response->expectsOutput("User with that email or username already exists");
        $response->expectsQuestion("What's the email of the user", $correctUser->email);
        $response->expectsQuestion("What's the username of the user", $correctUser->username);

        $response->expectsQuestion("What's the password of the user", 'password');
        $response->expectsQuestion("What's the name of the user", $correctUser->name);
        $response->expectsQuestion("What's the surname of the user", $correctUser->surname);
        $response->expectsQuestion("Do you want to add any description?", false);
        $response->expectsOutput("New user created properly");
    }

    public function testCreateUserWithoutData(){
        $response = $this->artisan('create:user');
        $response->expectsOutput("Welcome to user creation command");
        $response->expectsQuestion("What's the email of the user", "");
        $response->expectsQuestion("What's the username of the user", "");
        $response->expectsQuestion("What's the password of the user", "");
        $response->expectsQuestion("What's the name of the user", "");
        $response->expectsQuestion("What's the surname of the user", "");
        $response->expectsQuestion("Do you want to add any description?", false);
        $response->assertExitCode(1);
    }

    public function testCreateUserWithDescription(){
        $user = User::factory()->make([
            'description' => Str::random(50)
        ]);
        $response = $this->artisan('create:user');
        $response->expectsOutput("Welcome to user creation command");
        $response->expectsQuestion("What's the email of the user", $user->email);
        $response->expectsQuestion("What's the username of the user", $user->username);
        $response->expectsQuestion("What's the password of the user", 'password');
        $response->expectsQuestion("What's the name of the user", $user->name);
        $response->expectsQuestion("What's the surname of the user", $user->surname);
        $response->expectsQuestion("Do you want to add any description?", true);
        $response->expectsQuestion("What's the description of the user", $user->description);
        $response->expectsOutput("New user created properly");
    }
}
