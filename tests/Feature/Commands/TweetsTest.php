<?php

namespace Tests\Feature\Commands;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Tweet;
use App\Models\Like;

/**
 * 
 * @group commands
 * @group tweets_command
 * 
 * */

class TweetsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testTweetsCountWorks()
    {
        $howManyTweets = Tweet::count();
        $response = $this->artisan('tweets:count');
        $response->expectsOutput($howManyTweets);
    }

    public function testLikesCountWorks()
    {
        $howManyLikes = Like::count();
        $response = $this->artisan('likes:count');
        $response->expectsOutput($howManyLikes);
    }

    public function testCreateTweet(){
        $tweet = Tweet::factory()->make();
        $response = $this->artisan('create:tweet');
        $response->expectsOutput("Welcome to tweet creation command");
        $response->expectsQuestion("What's the username of the owner", $tweet->user->username);
        $response->expectsQuestion("Write the content of the tweet", $tweet->content);
        $response->expectsOutput("New tweet created properly");
    }


    // Testear que falla si el usuario no existe
    // Testear que falla si el contenido no se agrega
}
