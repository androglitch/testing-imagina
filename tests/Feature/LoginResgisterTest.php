<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;
use Str;


/**
 * 
 * @group login
 * @group http
 * 
 * */


class LoginRegisterTest extends TestCase
{

    /* Configuración global para todas las clases */

    private $_user;


    public function setUp(): void{
        parent::setUp();
        $this->withoutExceptionHandling();
        if(!$this->_user)
            $this->_user = User::factory()->create();
        
    }


    public function testLogin(){
        $route = route('api.login');
        $response = $this->postJson($route, [
            'email' => $this->_user->email,
            'password' => 'password'
        ]);

        $response->assertStatus(200);
    }

    public function testLoginFailedByPassword(){
        $route = route('api.login');
        $response = $this->postJson($route, [
            'email' => $this->_user->email,
            'password' => 'secret'
        ]);

        $response->assertStatus(401);
    }

    public function testLoginFailedByEmail(){
        $route = route('api.login');
        $response = $this->postJson($route, [
            'email' => $this->_user->email . 'a',
            'password' => 'password'
        ]);

        $response->assertStatus(401);
    }

    public function testLoginFailedBecauseNoPassword(){
        $route = route('api.login');
        $response = $this->postJson($route, [
            'email' => $this->_user->email
        ]);

        $response->assertStatus(401);
    }

    public function testLoginFailedBecauseNoEmail(){
        $route = route('api.login');
        $response = $this->postJson($route, [
            'password' => 'password'
        ]);

        $response->assertStatus(401);
    }


    public function _testRegister(){
        $newUser = User::factory()->make();
        $response = $this->postJson('/api/register', [
            'name' => $newUser->name,
            'username' => $newUser->username,
            'email' => $newUser->email,
            'password' => 'password'
        ]);
        //Comprobar que el usuario que he creado coincide con los datos que he enviado
        
        $content = $response->decodeResponseJson();
        $user = $content['data'];
        unset($user['id']);


        $this->assertTrue(\Hash::check('password',$user['password']));
        unset($user['password']);

        $response->assertStatus(200);
        $this->assertSame($newUser->toArray(), $user);
        
    }

    public function testRegisterFailed(){
        $response = $this->postJson('/api/register', [
            'email' => 'piper.schowalter@example.org',
            'password' => 'password'
        ]);
        $response->assertStatus(401);
    }

}
