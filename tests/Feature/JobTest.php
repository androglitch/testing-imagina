<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Jobs\TestingJob;
use App\Models\User;
use App\Http\Middleware\MiCheckThatNumberIsOneMiddleware;
/**
 * 
 * @group job
 * 
 * */
class JobTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function _test_example(){
        Mail::fake();
        //Mock de la clase que haga cosas raras
        $user = User::factory()->make()->toArray();
        $job = new TestingJob($user);
        $job->disptachSync();
        $jobSucess = User::where('email', $user['email'])->exists();
        $this->assertTrue($jobSucess);
    }


    public function _test_example_2(){
        Mail::fake();
        //Mock de la clase que haga cosas raras
        $user = User::factory()->make();
        $job = new TestingJob($user->toArray());
        $job->disptachSync();
        $this->assertDatabaseHas($user);
    }
    
}
