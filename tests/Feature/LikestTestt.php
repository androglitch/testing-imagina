<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Tweet;
use App\Models\Like;

/**
 * 
 * @group likes
 * @group http
 * 
 * */


class LikesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIfItWorks()
    {
        $user = User::find(1);
        $likes = Like::where('user_id', $user->id)->pluck('id')->toArray();

        $lastTweet = Tweet::where('user_id', '<>', $user->id)->whereNotIn('id', $likes)->orderBy('id', 'desc')->first();
        $identifier = $lastTweet->identifier;
        $response = $this->actingAs($user)->post('/api/likes/toggle/' . $identifier);
        $response->assertStatus(201);
    }

    public function testIfItFails()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->post('/api/likes/toggle/ASDF');
        $response->assertStatus(401);
    }

    public function testIfIMakeDislike()
    {
        $user = User::find(1);
        $lastTweet = Tweet::where('user_id', '<>', $user->id)->orderBy('id', 'desc')->first();
        $identifier = $lastTweet->identifier;
        Like::updateOrCreate([
            'user_id' => $user->id,
            'tweet_id' => $lastTweet->id
        ]);

        $response = $this->actingAs($user)->post('/api/likes/toggle/' . $identifier);
        $response->assertStatus(204);
    }

    public function testcheckIfItDisapearFormProfile(){
        $user = User::find(1);
        Like::where([
            'user_id' => $user->id,
        ])->delete();
        $response = $this->actingAs($user)->get('/api/profile/likes');
        $response->assertJsonCount(0);
    }
}
