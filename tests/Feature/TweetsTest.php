<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Tweet;
use App\Models\Comment;
use Str;
use Exception;


/**
 * 
 * @group tweets
 * @group http
 * 
 * */

class TweetsTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    private $_user;


    public function setUp(): void{
        parent::setUp();
        if(!$this->_user)
            $this->_user = User::factory()->create();
    }

    public function testStoreIdentifier(){
        $data = [
            'content' => Str::random(50)
        ];
        $response = $this->actingAs($this->_user)->postJson('/api/tweets', $data);
        $content = $response->decodeResponseJson();
        $response->assertStatus(201);
        $this->assertIsString($content['identifier']);
    }

    /**
     * 
     * @depends testStoreIdentifier
     * 
     * */


    public function testStore(){
        $data = [
            'content' => Str::random(50)
        ];
        $response = $this->actingAs($this->_user)->postJson('/api/tweets', $data);
        $content = $response->decodeResponseJson();
        $tweet = Tweet::where('identifier', $content['identifier'])->first()->toArray();
        $response->assertStatus(201);
        $response->assertExactJson($tweet);
        $this->assertSame($data['content'], $tweet['content']);
    }

    /**
     * 
     * @group list_tweets
     * 
     * */

    public function testGettingTweets(){
        $randomNumber = rand(1,10);
        Tweet::factory($randomNumber)->create([
            'user_id' => $this->_user->id
        ]);
        $tweets = Tweet::whereIn('user_id', $this->_user->following->pluck('id'))
                    ->orWhere('user_id', $this->_user->id)
                    ->with(['user'])
                    ->latest()
                    ->paginate(8)->withPath(env('APP_URL') . '/api/tweets')->toArray();
        $response = $this->actingAs($this->_user)->get('/api/tweets');
        $response->assertStatus(200);
        $response->assertJson($tweets);
    }


    /**
     * 
     * @group list_tweets
     * 
     * */

    public function testGettingOtherUserTweetsFails(){
        $randomNumber = rand(1,10);
        $secondUser = User::factory()->create();
        Tweet::factory($randomNumber)->create([
            'user_id' => $secondUser->id
        ]);
        $tweets = Tweet::whereIn('user_id', $secondUser->pluck('id'))
            ->orWhere('user_id', $secondUser->id)
            ->with(['user'])
            ->latest()
            ->paginate(8)->withPath(env('APP_URL') . '/api/tweets')->toArray();
        $response = $this->actingAs($this->_user)->get('/api/tweets');
        $response->assertStatus(200);
        $response->assertJsonMissingExact($tweets);
    }

    /**
     * 
     * @group list_tweets
     * 
     * */

    public function testGettingTweetsWhenNoAuthenticatedFails(){
        $randomNumber = rand(1,10);
        $secondUser = User::factory()->create();
        Tweet::factory($randomNumber)->create([
            'user_id' => $secondUser->id
        ]);
        $tweets = Tweet::whereIn('user_id', $secondUser->pluck('id'))
            ->orWhere('user_id', $secondUser->id)
            ->with(['user'])
            ->latest()
            ->paginate(8)->withPath(env('APP_URL') . '/api/tweets')->toArray();
        $response = $this->get('/api/tweets');
        $response->assertStatus(302);
    }


    public function testStoreInDatabaseWithLongIdentifierFails(){
        $identifier = Str::random(300);
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/' . $identifier, [
            'content' => 'Lorem Impsum',
            'identifier' => $identifier
        ]);
        $error = [
            'identifier' => ['Se ha excedido el tamaño del identificador']
        ];
        $response->assertStatus(404);
    }

    public function testUpdateSuccess(){
        $tweet = Tweet::factory()->create([
            'user_id' => $this->_user->id
        ]);
        $data = [
            'content' => Str::random(500)
        ];
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/' . $tweet->identifier, $data);
        $content = $response->decodeResponseJson();
        $tweetAfterUpdate = Tweet::where('identifier', $content['identifier'])->first()->toArray();
        $this->assertSame($data['content'], $tweetAfterUpdate['content']);
        $response->assertStatus(200);
        $response->assertExactJson($tweetAfterUpdate);
    }

    public function testStoreWithoutUserFails(){
        $response = $this->postJson('/api/tweets',[
            'content' => 'Lorem Ipsum'
        ]);
        $response->assertStatus(401);
    }

    public function testStoreInDatabaseWithoutContent(){
        $response = $this->actingAs($this->_user)->postJson('/api/tweets', [
            'content' => ''
        ]);
        $error = [
            'content' => ['No se puede crear un tweet vacío']
        ];
        $response->assertStatus(401);
        $response->assertExactJson($error);
    }

    public function testMakeRetweet(){
        $tweet = Tweet::factory()->create();
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/retweet/' . $tweet->identifier);
        $content = $response->decodeResponseJson();
        $retweeted = Tweet::where('identifier', $content['identifier'])->first();
        $this->assertInstanceOf(Tweet::class, $retweeted);
        $response->assertStatus(201);
        $response->assertExactJson($retweeted->toArray());
    }

    public function testMakeRetweetFails(){
        $invalidIdentifier = Str::uuid();
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/retweet/' . $invalidIdentifier);
        $response->assertStatus(404);
    }

    public function testMakeRetweetFailsForNoIdentifier(){
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/retweet');
        $response->assertStatus(404);
    }



    public function testMakeComment(){
        $tweet = Tweet::factory()->create();
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/comment/' . $tweet->identifier, [
            'content' => Str::random(100)
        ]);
        $content = $response->decodeResponseJson();
        $comment = Comment::where('id', $content['id'])->first()->toArray();
        $response->assertStatus(201);
        $response->assertExactJson($comment);
    }

    public function testThatMakeEmptyCommentFails(){
        $tweet = Tweet::factory()->create();
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/comment/' . $tweet->identifier, [
            'content' => ''
        ]);
        $error = [
            'content' => ['Debes especificar un contenido']
        ];
        $response->assertStatus(401);
        $response->assertExactJson($error);
    }

    public function testThatMakeNoCommentFails(){
        $tweet = Tweet::factory()->create();
        $response = $this->actingAs($this->_user)->postJson('/api/tweets/comment/' . $tweet->identifier);
        $response->assertStatus(401);
    }

    public function testThatRemoveTweetWorks(){
        $tweet = Tweet::factory()->create([
            'user_id' => $this->_user->id
        ]);
        $response = $this->actingAs($this->_user)->delete('/api/tweets/' . $tweet->identifier);
        $exists = Tweet::where('identifier', $tweet->identifier)->exists();
        $this->assertFalse($exists);
        $response->assertStatus(204);
    }

    public function testThatRemoveTweetsOfOtherUsersFails(){
        $secondUser = User::factory()->create();
        $tweet = Tweet::factory()->create([
            'user_id' => $secondUser->id
        ]);
        $response = $this->actingAs($this->_user)->delete('/api/tweets/' . $tweet->identifier);
        $exists = Tweet::where('identifier', $tweet->identifier)->exists();
        $response->assertStatus(404);
        $this->assertTrue($exists);
    }

    public function testThatRemoveNotExistingTweetsFails(){
        $invalidIdentifier = Str::uuid();
        $response = $this->actingAs($this->_user)->delete('/api/tweets/' . $invalidIdentifier);
        $exists = Tweet::where('identifier', $invalidIdentifier)->exists();
        $response->assertStatus(404);
        $this->assertFalse($exists);
    }

    public function testThatRemoveAlreadyDeletedTweetsFails(){
        $tweet = Tweet::factory()->create([
            'user_id' => $this->_user->id
        ]);
        $tweet->delete();
        $response = $this->actingAs($this->_user)->delete('/api/tweets/' . $tweet->identifier);
        $exists = Tweet::where('identifier', $tweet->identifier)->exists();
        $this->assertFalse($exists);
        $response->assertStatus(404);
    }
}


















