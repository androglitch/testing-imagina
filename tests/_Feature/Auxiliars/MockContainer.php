<?php

namespace Tests\Feature\Auxiliars;

use Mockery;
use Mockery\MockInterface;
use App\Models\User;

class MockContainer{


	public static function getUserMock(){
        return Mockery::mock(User::class, function (MockInterface $mock) {
            $mock->shouldReceive('calculateSomething')->andReturn(5)->times(1);
        });
	}

    public static function googleAuthFail(){
        
    }
}