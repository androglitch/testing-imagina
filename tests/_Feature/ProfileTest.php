<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\Models\User;
use Storage;


class ProfileTest extends TestCase
{

    public function testProfile(){
        $user = User::find(1);
        $response = $this->actingAs($user)->get('/api/profile');
        $response->assertStatus(200);
    }

    public function testProfileForbiden(){
        $response = $this->withHeaders([
            'Authorization' => 'Invalid Token'
        ])->get('/api/profile');
        $response->assertStatus(302);
    }

    public function testProfileResponse(){
        $user = User::find(1);
        $json = $user->toArray();
        $response = $this->actingAs($user)->get('/api/profile');
        $response->assertExactJson($json, true);
    }

    public function _testUpdateAvatar(){
        $user = User::find(1);
        $file = UploadedFile::fake()->image('avatar.jpg');
        $response = $this->post('/api/profile/update-avatar',[
            'avatar' => $file
        ]);
        Storage::disk('public')->assertExists($file->hashName());
    }

}
