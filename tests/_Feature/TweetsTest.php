<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Tweet;
use Str;

class TweetsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */


    
    public function testStore()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->postJson('/api/tweets', [
            'content' => 'Lorem Ipsum'
        ]);
        $response->assertStatus(201);
    }


    public function testStoreInDatabase()
    {
        $user = User::find(1);

        $response = $this->actingAs($user)->postJson('/api/tweets', [
            'content' => 'Lorem Ipsum'
        ]);
        // Obtener la respuesta de vuelta
        $content = $response->decodeResponseJson();
        $tweet = Tweet::where('identifier', $content['identifier'])->first()->toArray();
        $response->assertExactJson($tweet);
    }

    public function testStoreInDatabaseWithLongIdentifier()
    {
        $user = User::find(1);
        $identifier = Str::random(300);
        $response = $this->actingAs($user)->postJson('/api/tweets/' . $identifier, [
            'content' => 'Lorem Ipsum',
            'identifier' => $identifier
        ]);
        $response->assertStatus(401);
    }

    public function testStoreInDatabaseWithoutContent()
    {
        $user = User::find(1);
        $response = $this->actingAs($user)->postJson('/api/tweets', [
            'content' => ''
        ]);
        $response->assertStatus(401);
        
    }

    public function testStoreWithoutUser()
    {
        $response = $this->postJson('/api/tweets', [
            'content' => 'Lorem Ipsum'
        ]);
        $response->assertStatus(401);
    }

}
