<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\User;
use Str;

class LoginRegisterTest extends TestCase
{

    /* Configuración global para todas las clases */


    public function testLogin(){
        $route = route('api.login');
        $response = $this->postJson($route, [
            'email' => 'piper.schowalter@example.org',
            'password' => 'password'
        ]);

        $response->assertStatus(200);
    }

    public function testLoginFailed(){
        $route = route('api.login');
        $response = $this->postJson($route, [
            'email' => 'piper.schowalter@example.gor',
            'password' => 'secret'
        ]);

        $response->assertStatus(401);
    }


    public function testRegister(){
        $response = $this->postJson('/api/register', [
            'name' => Str::random(10),
            'username' => Str::random(10),
            'email' => Str::random(10).'@imagina.net',
            'password' => 'password'
        ]);

        $response->assertStatus(200);
    }

    public function testRegisterFailed(){
        $response = $this->postJson('/api/register', [
            'email' => 'piper.schowalter@example.org',
            'password' => 'password'
        ]);

        $response->assertStatus(401);
    }

}
