<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * 
 * @group console
 * 
 * */

class CommandsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIspire(){
        $response = $this->artisan('inspire');
        $response->assertSuccessful();
    }


    public function testAsk(){
        $response = $this->artisan('testing:ask');
        $response->expectsQuestion('¿Cuántos años tienes?', '23');
        $response->expectsQuestion('¿Cuál es tu nombre?', 'Jose');
        $response->expectsQuestion('¿Con qué lenguaje programas?', 'PHP');
        $response->doesntExpectOutput("Tu nombre es Jose te gusta Python y tienes 23 años");
        $response->expectsConfirmation('¿Es correcto?', true);
        $response->expectsOutput("Este es mi 2º line");
        $response->assertExitCode(0);   
    }


    public function testMake(){
        $response = $this->artisan('make:controller ExampleController');
    }
}
