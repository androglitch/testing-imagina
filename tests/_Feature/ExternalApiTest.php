<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

/**
 * 
 * @group api
 * 
 * */

class ExternalApiTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        Http::fake([
            'http://127.0.0.1:8000/api/tweets/test' => Http::response(
                [
                    'response' => "Hello World",
                    'success' => true
                ],
                200,
                [
                    'Content-type' => 'application/json'
                ])
        ]);
        $response = Http::get('http://127.0.0.1:8000/api/tweets/test');
        $result = ['response' => "Hello Worlds", 'success' => true];
        $this->assertEquals($result, $response->json());
    }
}
