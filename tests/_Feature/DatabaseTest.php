<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Tweet;
use App\Models\User;
use Artisan;

/**
 * 
 * @group database
 * */

class DatabaseTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateFifty()
    {
        $firstUserCreation = User::factory(50)->create();
        $this->assertDatabaseCount(User::class, 50);
    }

    public function testNonCreateFifty(){
        $firstUserCreation = User::factory(1)->create();
        $user = User::find($firstUserCreation[0]['id']);
        $this->assertModelExists($user);
    }

    public function testGetFullName(){
        $firstUserCreation = User::factory(1)->create([
            'name' => 'George',
            'surname' => 'Orwell'
        ]);
        $user = User::find($firstUserCreation[0]['id']);
        $this->assertSame('George Orwell', $user->fullname);
    }

    public function testHashedPassword(){
        $firstUserCreation = User::factory(1)->create([
            'password' => 'secret'
        ]);
        $user = User::find($firstUserCreation[0]['id']);
        $hashed = \Hash::check('secret', $user->password);
        $this->assertTrue($hashed);
    }

    public function testCustomMethod(){
        $firstUserCreation = User::factory(1)->create([
            'password' => 'secret'
        ]);
        $user = User::find($firstUserCreation[0]['id']);
        $result = $user->calculateSomething();
        $this->assertSame(4, $result);
    }

    public function testCreateTweets(){
        $user = User::first();
        $firstUserCreation = Tweet::factory(50)->for($user)->create();
        $this->assertTrue(true);
    }

}
